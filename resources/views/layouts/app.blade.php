<!DOCTYPE html>
<html lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>LTV Sports</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <!-- Custom CSS -->
        <link rel='stylesheet' id='coupon-owl.carousel-css'  href='{{ asset('css/owl.carousel.css')}}' type='text/css' media='all' />
        <link rel='stylesheet' id='coupon-owl.carousel-theme-css'  href='{{ asset('css/owl.theme.default.min.css')}}' type='text/css' media='all' />
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <section class="wrapper">
            <!-- .warpper -->

            @include('layouts.headers')
            @yield('content')
            @include('layouts.footer')

        </section>
        <!-- /.wrapper -->
        <!-- jQuery -->
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <!--Owl carousal-->
        <script type='text/javascript' src='{{asset('js/owl.carousel.min.js')}}'></script>
        <!-- Bootstrap JavaScript -->
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="{{asset('js/main.js')}}"></script>
    </body>

</html>