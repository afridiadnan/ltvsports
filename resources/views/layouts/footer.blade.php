@include ('layouts.modal')

<!-- footer -->
<footer class="main-footer">
    <div class="custom-container">
        <div class="row">
            <div class="col-md-8 center-content">
                <div class="row">
                    <div class="col-xs-4 mobile-block">
                        <h3>Quick Links</h3>
                        <ul class="list-unstyled">
                            <li><a href="#"><img src="{{asset('images/feedback.svg')}}" alt="" class="icons">Feedback</a></li>
                            <li><a href="#"><img src="{{asset('images/about-icon-01.svg')}}" alt="" class="icons">About us</a></li>
                            <li><a href="#"><img src="{{asset('images/trems.svg')}}" alt="" class="icons">Terms and conditions</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4 mobile-block">
                        <h3>Follow Us</h3>
                        <ul class="list-unstyled">
                            <li><a href="#"><img src="{{asset('images/facebook_footer.svg')}}" alt="" class="icons">Facebook</a></li>
                            <li><a href="#"><img src="{{asset('images/twitter_footer.svg')}}" alt="" class="icons">Twitter</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4 mobile-block">
                        <h3>Download App</h3>
                        <ul class="list-unstyled">
                            <li><a href="#"><img src="{{asset('images/android_footer.svg')}}" alt="" class="icons">Android App</a></li>
                            <li><a href="#"><img src="{{asset('images/iOS_footer.svg')}}" alt="" class="icons">iOS App</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p class="copy-rights">© All Right Reserved LTV Sports 2017</p>
            </div>
        </div>
    </div>
</footer>
<!-- /footer -->