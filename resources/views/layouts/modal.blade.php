<!--login popup-->
<div class="login-register-modal">
    <div class="modal fade primary-modal" id="login-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close-btn" data-dismiss="modal"><img src="images/close-icon.svg" alt=""></button>
                    <h2 class="modal-heading">Welcome</h2>
                    <p class="modal-para">Lorem ipsum asdlnalsk lknalsdkna lsndljasd Lorem ipsum asdlnalsk lknalsdkna lsnd</p>
                    <form action="" method="POST" class="form-horizontal text-center" role="form">
                        <div class="form-group">
                            <input type="text" name="user-name" id="user-name" class="form-control round-input" value="" required="required" pattern="" title="" placeholder="username">
                            <input type="password" name="passowrd" id="passwrod" class="form-control round-input" value="" required="required" pattern="" title="" placeholder="Password">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                        <div class="form-group remember-field">
                            <div class="col-sm-6">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox2" type="checkbox" checked="">
                                    <label for="checkbox2">
                                        Remember me
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <a href="#">Forgot Password</a>
                            </div>
                        </div>

                    </form>
                    <h2 class="line-through m-t-47 m-b-30">
                        <span>Or Login with</span>
                    </h2>
                    <div class="clearfix text-left">
                        <a href="#" class="f-btn socail-btn ">
                            <img src="images/f-icon.svg" alt="">
                            Facebook
                        </a>
                        <a href="#" class="g-btn socail-btn pull-right">
                            <img src="images/g-plus-icon.svg" alt="">
                            Google
                        </a>
                    </div>
                    <p class="signin-para">Don't have an account? <a class="btn-primary btn" data-dismiss="modal"  data-toggle="modal" href='#register-modal'>SignUp</a></p>
                </div>

            </div>
        </div>
    </div>
</div>

<!--registration popup-->
        <div class="login-register-modal">
            <div class="modal fade primary-modal" id="register-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body text-center">
                        <button type="button" class="close-btn" data-dismiss="modal"><img src="images/close-icon.svg" alt=""></button>
                            <h2 class="modal-heading">Register</h2>
                            <p class="modal-para">Lorem ipsum asdlnalsk lknalsdkna lsndljasd Lorem ipsum asdlnalsk lknalsdkna lsnd</p>
                            <form action="" method="POST" class="form-horizontal text-center" role="form">
                                    <div class="form-group">
                                        <input type="text" name="f-name" id="f-name" class="form-control round-input" value="" required="required" pattern="" title="" placeholder="Full Name">
                                        <input type="email" name="email" id="email" class="form-control round-input" value="" required="required" pattern="" title="" placeholder="Email">
                                        <input type="password" name="passowrd" id="password" class="form-control round-input" value="" required="required" pattern="" title="" placeholder="Password">
                                        <input type="password" name="confirm-passowrd" id="confirm-passowrd" class="form-control round-input" value="" required="required" pattern="" title="" placeholder="Confirm Password">
                                        <button type="submit" class="btn btn-primary">Login</button>
                                    </div>
                                    
                            </form>
                            <h2 class="line-through m-t-47 m-b-30">
                                <span>Or Login with</span>
                            </h2>
                            <div class="clearfix text-left">
                                <a href="#" class="f-btn socail-btn ">
                                    <img src="images/f-icon.svg" alt="">
                                    Facebook
                                </a>
                                <a href="#" class="g-btn socail-btn pull-right">
                                    <img src="images/g-plus-icon.svg" alt="">
                                    Google
                                </a>
                            </div>
                            <p class="signin-para">Already have an account? <a class="btn-primary btn" data-dismiss="modal" data-toggle="modal" href='#login-modal'>Signin</a></p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>