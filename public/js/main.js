/**
 PRIMARY ACORDEON
 */

function toggleChevron(e) {
    $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);

$("#top-calender").owlCarousel({
    loop: true,
    items: 3,
    margin: 5,
    nav: true,
    autoplay: false,
    smartSpeed: 500,
});