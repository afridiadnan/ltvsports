<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NewsDetail;

class News extends Model {

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'updated_at'];
    public $timestamps = false;

    public function details() {
        return $this->hasOne(NewsDetail::class);
    }

}
