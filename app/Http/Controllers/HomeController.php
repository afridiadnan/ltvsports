<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller {

    private $cricket;
    private $football;
    private $nba;

    public function __construct() {

        DB::enableQueryLog();
        $this->cricket = new SportsController();
        $this->football = new FootballController();
        $this->nba = new NbaController();
    }

    public function index(Request $request) {

        $request->request->add(['device_token' => 'abcd']);

        $result = $this->cricket->newsList($request);

        $data = json_decode($result->content(), true);
// dd($data);
        return view('cricket.home', $data);
    }

    public function cricketHome(Request $request) {

        $request->request->add(['device_token' => 'abcd']);

        $result = $this->cricket->cricketInfo($request);

        $data = json_decode($result->content(), true);

        return view('cricket.index', $data);
    }

    public function footballHome(Request $request) {

        $request->request->add(['device_token' => 'abcd']);

        $result = $this->football->cupScores($request);

        $data = json_decode($result->content(), true);

        return view('football.index', $data);
    }

    public function nbaHome(Request $request) {

        $request->request->add(['device_token' => 'abcd']);

        $result = $this->nba->cupScores($request);

        $data = json_decode($result->content(), true);
//dd($data);
        return view('nba.index', $data);
    }

    public function newsDetail(Request $request) {

        $newsLink = $request->input('link');

        $request->request->add(['device_token' => 'idki934949', 'news_link' => $newsLink]);

        $result = $this->cricket->newsDetail($request);

        $data = json_decode($result->content(), true);
//        dd($data);
        return view('cricket.news-detail', $data);
    }

    public function scoreBoard(Request $request) {

        $request->request->add(['device_token' => 'abc123', 'match_link' => $request->input('link')]);
//        dd($request->all());
        $result = $this->cricket->scoreBoard($request);

        $data = json_decode($result->content(), true);

        return view('cricket.scoreboard', $data);
    }

    public function summary(Request $request) {
        
        $data['summary'] = [];
        $data['scoreBoard'] = [];
        $data['commentary'] = [];

        $matchLink = $request->input('link');

        $request->request->add(['device_token' => 'abc123', 'match_url' => $matchLink]);
//        dd($request->all());
//        summary
        $summary = $this->cricket->summary($request);
        $data['summary'] = json_decode($summary->content(), true);


//scoreBoard
        $scoreBoard = $this->cricket->scoreBoard($request);
        $data['scoreBoard'] = json_decode($scoreBoard->content(), true);

//Commentary
        $commentary = $this->cricket->commentary($request);
        $data['commentary'] = json_decode($commentary->content(), true);


//dd($data);
        return view('cricket.summary', $data);
    }

    public function footBallSummary(Request $request) {
        
        $data['summary'] = [];
        $data['commentary'] = [];
        $data['squad'] = [];

        $matchLink = $request->input('link');

        $request->request->add(['device_token' => 'abc123', 'match_url' => "$matchLink"]);
//        dd($request->all());
//        summary
        $summary = $this->football->summary($request);
        $data['summary'] = json_decode($summary->content(), true);


//Commentary
        $commentary = $this->football->commentary($request);
        $data['commentary'] = json_decode($commentary->content(), true);

//squad
        $squad = $this->football->squad($request);
        $data['squad'] = json_decode($squad->content(), true);
//dd($data);

        return view('football.summary', $data);
    }

    public function nbaSummary(Request $request) {
        
        $data['summary'] = [];
        $data['commentary'] = [];
        $data['squad'] = [];

        $matchLink = $request->input('link');

        $request->request->add(['device_token' => 'abc123', 'match_url' => "$matchLink"]);
//        dd($request->all());
//        summary
        $summary = $this->nba->summary($request);
        $data['summary'] = json_decode($summary->content(), true);


//Commentary
        $commentary = $this->nba->commentary($request);
        $data['commentary'] = json_decode($commentary->content(), true);

////squad
//        $squad = $this->football->squad($request);
//        $data['squad'] = json_decode($squad->content(), true);
//dd($data);

        return view('nba.summary', $data);
    }

}
